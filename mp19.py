#!/usr/bin/python


primes=[]

#Function for finding primes below 'number'
def prime_finder(number):
    for num in range(1, number + 1):
        if num > 1:
            for i in range(2, num):
            #see if number has factors
                if (num % i) == 0:
                    #stop if it has any factors
                    break
            else:
                #Add number to prime-list
                primes.append(num)

#Check a number fits the challenge
def sqyared(x):
    a=0
    
    #Square the first 'x' primes and add them together
    for p in range(x):
        a=a+primes[p]**2
    
    if a % x ==0:
        print(x)


def thingie(prime):
    for c in range(2,prime):
            sqyared(c)

def main(x,y):
    prime_finder(x)
    print('Primes that clears the challenge:')
    thingie(y)
    

main(10000,600)


#
# Output of main(10000,600)
#
# Primes that clears the challenge
# 1
# 19
# 37
# 455
# 509
# 575

